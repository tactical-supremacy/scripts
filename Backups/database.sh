#!/usr/local/bin/bash

# set -x

MY_USER="backups"
MYSQLDUMP_OPTS="--opt -AY"
KEEP_DAYS="30"

# Comma-separated list, no spaces
ADMINS=""

DATE=$(date +%Y%m%d)
MYSQLDUMP="/usr/local/bin/mysqldump"
BZIP2="/usr/bin/bzip2"

DUMP_DIR="$HOME/mysql-backups"
DUMP_FILE="tacticallychallenged-mysql-${DATE}.sql"

function alert_admins {
        df=$(/bin/df -h)

        echo | mail ${ADMINS} <<EOM
From: TacticallyChallenged Backups <noreply@tacticallychallenged.club>
Subject: TacticallyChallenged backup failure!

MySQL backups failed on ${DATE} for some reason.

Please investigate this matter.

${df}

EOM
}

function prune_old_backups {
        find ${DUMP_DIR} \
                -type f \
                -mtime +${KEEP_DAYS} \
                | xargs rm -f
}

###
## Main
###

test -d ${DUMP_DIR} || mkdir -p ${DUMP_DIR}
rm -f ${DUMP_DIR}/${DUMP_FILE} ${DUMP_DIR}/${DUMP_FILE}.bz2

${MYSQLDUMP} -u ${MY_USER} \
        ${MYSQLDUMP_OPTS} \
        > ${DUMP_DIR}/${DUMP_FILE}

if [ $? -eq 0 ]; then
        ${BZIP2} ${DUMP_DIR}/${DUMP_FILE}
else
        alert_admins
fi

prune_old_backups
