from aadiscordbot.tasks import send_message
from corptools.models import *
location_ids = [] # put IDs in the array
pingers = {}
implants = JumpClone.objects.filter(location_name_id__in=location_ids).select_related('character', 'character__character', 'character__character__character_ownership', 'character__character__character_ownership__user', 'character__character__character_ownership__user__profile__main_character').prefetch_related('implant_set', 'implant_set__type_name')
for imp in implants:
    if imp.implant_set.count() > 0:
        try:
            if imp.character.character.character_ownership.user.discord.uid not in pingers:
                pingers[imp.character.character.character_ownership.user.discord.uid] = []
            pingers[imp.character.character.character_ownership.user.discord.uid].append(imp.character.character.character_name)
        except:
            print("{} - no discord".format(imp.character.character))
    else:
        print(f"{imp.character.character} Empty")

message = "{} - Have Jump Clones currently in in DO6-H with Implants. \n\nPlease see pings."
for user_id, chars in pingers.items():
    print(message.format(", ".join(chars)))
    send_message(user_id=user_id, message=message.format(", ".join(chars)))
