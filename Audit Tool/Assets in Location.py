from aadiscordbot.tasks import send_message
from corptools.models import *
location_ids = [] # put IDs in the array
group_ids = [30, 547, 659, 1538, 485, 902, 513, 883, 77283] #preselected caps
pingers = {}
ships = CharacterAsset.objects.filter(location_name_id__in=location_ids, type_name__group_id__in=group_ids).select_related('character', 'character__character', 'character__character__character_ownership', 'character__character__character_ownership__user', 'character__character__character_ownership__user__profile__main_character')
for ship in ships:
    try:
        if ship.character.character.character_ownership.user.discord.uid not in pingers:
            pingers[ship.character.character.character_ownership.user.discord.uid] = []
        if ship.character.character.character_name not in pingers[ship.character.character.character_ownership.user.discord.uid]:
            pingers[ship.character.character.character_ownership.user.discord.uid].append(ship.character.character.character_name)
    except:
        print("{} - no discord".format(ship.character.character))

message = "{} - Have Capitals in DO6-H. \n\nPlease see alliance pings as a matter of urgency."
for user_id, chars in pingers.items():
    print(message.format(", ".join(chars)))
    send_message(user_id=user_id, message=message.format(", ".join(chars)))
